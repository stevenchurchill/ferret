/**
 * @file   DepolEnergy.C
 * @author J. Mangeri <john.mangeri@uconn.edu>
 *
 * @brief  Implement the kernel for polar variables corresponding to ferroelectic depolarization
 *         energy after the variational derivative of the polar dependent terms have been taken.
 */

#include "DepolEnergy.h"

class DepolEnergy;

template<>
InputParameters validParams<DepolEnergy>()
{
  InputParameters params = validParams<Kernel>();
  params.addRequiredCoupledVar("polar_z", "The z component of the polarization");
  params.addParam<Real>("len_scale", 1.0, "the len_scale of the unit");
  params.addParam<PostprocessorName>("avePz", "the average polarization due to the depol field term");
  params.addParam<Real>("lambda", 1.0, "the screening length term");
  params.addParam<Real>("permitivitty", 1.0, "the permitivitty term");
  return params;
}

DepolEnergy::DepolEnergy(const InputParameters & parameters)
  :Kernel(parameters),
   _polar_z_var(coupled("polar_z")),
   _polar_z(coupledValue("polar_z")),
   _len_scale(getParam<Real>("len_scale")),
   _avePz(getPostprocessorValue("avePz")),
   _lambda(getParam<Real>("lambda")),
   _permitivitty(getParam<Real>("permitivitty"))
{
}

Real
DepolEnergy::computeQpResidual()
{
  return 0.5 * _lambda * (1.0 / _permitivitty) * _avePz * _test[_i][_qp] * std::pow(_len_scale, 3.0);
}

Real
DepolEnergy::computeQpJacobian()
{
  return 0.0;
}
